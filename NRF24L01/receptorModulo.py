# -*- coding: utf-8 -*-

#Llamadas a las ibrerías
import RPi.GPIO as GPIO
from lib_nrf24 import NRF24
import time
import spidev
import sqlite3 as lite
import serial

datosSensor = []
datosBool = []

for i in range(0,9):
    datosSensor.append(1) 
for i in range(0,8):
    datosBool.append(False) 

ser = serial.Serial('/dev/ttyUSB0', 9600)
def fromSerial():
    """Lee el puerto serial, datos que provienen de la estación de tierra"""
    valor = ser.readline()
    try:
        cadena = valor.decode("utf-8")
        #print (cadena)
        return cadena
    except UnicodeDecodeError:
        print("No se pudo decodificar")
        return 'ZZZZZZZ'
        

def trata(cade, magnitudString, retro, index):
    """Función que procesa los datos enviados desde la estación de tierra"""
    if cade[retro] == magnitudString:
        cade = cade.replace(magnitudString, ' ')
        cade = cade.strip()
        datosSensor[index] = float(cade)
        datosBool[index] = True
        try:
            datosSensor[8] = "Puebla, Puebla. MX"
        except IndexError:
            print("El arreglo está vacío")
        print(datosSensor[index])
        
def trataListas(cade, magnitudString, retro, index):
    """Función que procesa los datos enviados desde el módulo de sensores"""
    if cade[retro] == magnitudString:
        cade.pop()
        cade = ''.join(cade)
        datosSensor[index] = float(cade)
        datosBool[index] = True
        try:
            datosSensor[8] = "Puebla, Puebla. MX"
        except IndexError:
            print("El arreglo está vacío")
        print(datosSensor[index])

def guardar(sensorTup, boolList):
    """"Esta función guarda los datos del sensor en la base de datos usa banderas para saber si los datos fueron recibidos"""
    if boolList[0] and boolList[1] and boolList[2] and boolList[3] and boolList[4] and boolList[5] and boolList[6] and boolList[7] and len(sensorTup)==9:
        sql = '''INSERT INTO clima values(date('now'), time('now'), (?), (?), (?), (?), (?), (?), (?), (?), (?))'''
        db = lite.connect('AndraDataBase.db')
        cursor = db.cursor()
        sensorTup = tuple(sensorTup)
        cursor.execute(sql, sensorTup)
        db.commit()
        db.close()
        try:
            for i in range(0,8):
                datosBool[i] = False 
        except IndexError:
            print("La lista está vacía probablemente")


#Base de datos
db = lite.connect('AndraDataBase.db')
cursor = db.cursor()
cursor.execute('''CREATE TABLE IF NOT EXISTS clima( tdate DATE, ttime TIME, tempertura REAL, Hum REAL, CO2 REAL, absPress REAL, altitud REAL, seaLevelPress REAL, velViento REAL, watts REAL, zona TEXT)''')
#GPIO
GPIO.setmode(GPIO.BCM)

#Inicialización de la comunicación entre los NRF24L01
pipes = [[0xE8, 0xE8, 0xF0, 0xF0, 0xE1],[0xF0, 0xF0, 0xF0, 0xF0, 0xE1]]
radio =  NRF24(GPIO, spidev.SpiDev())
radio.begin(0, 17)
radio.setPayloadSize(32)
radio.setChannel(0x76)
radio.setDataRate(NRF24.BR_1MBPS)
radio.setPALevel(NRF24.PA_MIN)
radio.setAutoAck(True)
radio.enableDynamicPayloads()
radio.enableAckPayload()
radio.openReadingPipe(1, pipes[1])
radio.printDetails()
radio.startListening()


while 1:

    #Recepción de los datos NRF24L01
    while not radio.available(0):
        time.sleep(100/1000)
    receiveData = ["0000000"]
    recv_buffer=[]
    radio.read(recv_buffer, radio.getDynamicPayloadSize())
    receiveData[0] = ''.join(chr(o) for o in recv_buffer)
    receiveData[0] = list(receiveData[0])
    count = 0    
    for i in range(0, len(receiveData[0])):
        if receiveData[0][i] ==  '\x00' :
            count += 1
    for e in range(count):
        receiveData[0].remove('\x00')

    #Tratamiento de datos del módulo de sensores
    recibido = receiveData[0]
    trataListas(recibido, 'T', -1, 0)
    trataListas(recibido, 'H', -1, 1)
    trataListas(recibido, 'P', -1, 2)
    trataListas(recibido, 'C', -1, 3)
    trataListas(recibido, 'S', -1, 4)
    trataListas(recibido, 'A', -1, 5)
    #Tratamiento de datos de la estación de tierra
    tempo = fromSerial()
    trata(tempo, 'K' , -3, 6)
    trata(tempo, 'W' , -3, 7)

    guardar(datosSensor, datosBool)


    #trata(tempo, 'R' , -3)
    #trata(tempo, 'E' , -3)
    #trata(tempo, 'X' , -3)
"""
Temperatura 1
Humedad 2
Presión 3
CO2 4
Presión mar 5
Altura 6
Watts 7
velocidad del viento 8
Zona actual 9
"""

#/dev/ttyUSB0   Nano

#/dev/ttyACM0  Uno
