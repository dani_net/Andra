#!/usr/bin/env python
# -*- coding: UTF-8 -*-a
import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtMultimedia import *
import time
class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()


        #Menu bar and toolbar

        #Exit Action
        exitAction = QAction(QIcon('Button-Close-icon.png'), 'Cerrar', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Salir de la aplicación')
        exitAction.triggered.connect(self.close)

        #Help Action
        helpAction = QAction(QIcon('helpIcon.png'), 'Ayuda', self)
        helpAction.setShortcut('Ctrl+H')
        helpAction.setStatusTip('Ayuda')
        helpAction.triggered.connect(self.helpfile)

        #Back Action
        backAction = QAction(QIcon('arrow-back-icon.png'), 'Regresar', self)
        backAction.setShortcut('Ctrl+B')
        backAction.setStatusTip('Regresar al menú')
        backAction.triggered.connect(self.toMenu)

        self.statusBar()

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&Archivo')
        fileMenu.addAction(exitAction)
        editMenu = menubar.addMenu('&Editar')
        viewMenu = menubar.addMenu('&Ver')
        toolsMenu = menubar.addMenu('&Herramientas')
        helpMenu = menubar.addMenu('&Ayuda')
        helpMenu.addAction(helpAction)


        toolbar = self.addToolBar('Cerrar')
        toolbar.addAction(exitAction)
        backBar = self.addToolBar('Regresar')
        backBar.addAction(backAction)

        self.stack1 = QWidget()
        self.stack2 = QWidget()
        self.stack3 = QWidget()
        self.vueloControlado = QWidget()

        self.stack1UI()
        self.stack2UI()
        self.stack3UI()
        self.vueloControl()
        self.Stack = QStackedWidget (self)
        self.Stack.addWidget (self.stack1)
        self.Stack.addWidget (self.stack2)
        self.Stack.addWidget (self.stack3)
        self.Stack.addWidget (self.vueloControlado)
        hbox = QHBoxLayout(self)
        #hbox.addWidget(self.leftlist)
        hbox.addWidget(self.Stack)
        self.setCentralWidget(self.Stack)

        self.setLayout(hbox)


        self.display(1)
        self.setGeometry(0,0, 1200,350)
        self.setWindowTitle('Andra')
        self.show()


    def stack1UI(self):
        layout = QGridLayout()

        names = ['', 'Lunes', 'Martes', 'Miercoles', 'jueves', 'Viernes',
                 'Temperatura', '24 °', '29 °', '27 °', '25 °', '24 °',
                 'Humedad', '33 %', '34 %', '31 %', '38 %', '34 %',
                ]
        positions = [(i,j) for i in range(3) for j in range(6)]

        for position, name in zip(positions, names):
            if name == '':
                continue
            letrero = QLabel()
            letrero.setText(name)
            letrero.setStyleSheet('font-size: 24px;')
            layout.addWidget(letrero, *position)
        self.stack1.setLayout(layout)
    #Vista usada para el menu princial
    def stack2UI(self):
        layout = QGridLayout()

        names = ['', 'Vuelo Controlado', '',
                 '', 'Predicción', '',
                 '', 'Vuelo Autónomo', ''
                ]
        positions = [(i,j) for i in range(3) for j in range(3)]

        for position, name in zip(positions, names):
            if name == '':
                continue
            button = QPushButton(name)
            if name == 'Predicción':
                btnPred = button
                btnPred.clicked.connect(self.toPred)
            if name == 'Vuelo Controlado':
                btnControl = button
                btnControl.clicked.connect(self.toControl)
            button.setFixedHeight(60)
            button.setFixedWidth(250)
            layout.addWidget(button, *position)


        self.stack2.setLayout(layout)

    def stack3UI(self):
        layout = QVBoxLayout()
        btnInicio = QPushButton("Iniciar")
        btnInicio.setFixedHeight(60)
        btnInicio.setFixedWidth(150)

        pixmap = QPixmap("propellerLogo.png")
        lbl = QLabel(self)
        lbl.setPixmap(pixmap)

        layout.addWidget(lbl)
        horiLay = QHBoxLayout()
        horiLay.addWidget(btnInicio)
        layout.addLayout(horiLay)
        btnInicio.clicked.connect(self.toMenu)
        self.stack3.setLayout(layout)
    #Vista usada para indicar al operador el funcionamiento del control para el vuelo manual
    def vueloControl(self):
        layout = QGridLayout()

        imagenes = ['','image','']

        positions = [(i,j) for i in range(1) for j in range(3)]

        for position, imagen in zip(positions, imagenes):
            #if imagen == '':
                #continue
            #btnImage= QPushButton(imagen)
            if imagen == '':
                #btnImage = QPushButton(imagen)
                btnImage = QLabel(self)
                btnImage.setText('')
            if imagen == 'image':
                #btnImage = QLabel(self)
                #btnImage.setText('nothing')
                btnImage = QLabel(self)
                pixmap = QPixmap('xboxController.png')
                btnImage.setPixmap(pixmap)
            layout.addWidget(btnImage, *position)


            self.vueloControlado.setLayout(layout)

    def display(self,i):
        self.Stack.setCurrentIndex(i)

    def toMenu(self):
        self.Stack.setCurrentIndex(1)

    def toPred(self):
        self.Stack.setCurrentIndex(0)

    def toControl(self):
        self.Stack.setCurrentIndex(3)

    #esta función abre un documento en consola que provee los datos de contacto de la empresa por si se presenta algún problema con el equipo
    def helpfile(self):
        helpDoc = open('helpDoc.txt','r').read()

        print(helpDoc)

def main():
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()